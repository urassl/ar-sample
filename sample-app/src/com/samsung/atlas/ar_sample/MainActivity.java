package com.samsung.atlas.ar_sample;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import com.samsung.atlas.ar_sample.R;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

public class MainActivity extends Activity implements CvCameraViewListener2 {
    private static final String  TAG                 = "3DReconstructor";

    // Объект OpenCV для работы с камерой
    private CameraBridgeViewBase mOpenCvCameraView;
    
    // ======================
    // Open GL stuff
    private GLSurfaceView mGlSurfaceView;
    private GlRenderer mRenderer;
    private FrameLayout mPosesView;
    // ======================
    
    // Кнопки вращения траектории камеры
    private ImageButton mLeft, mRight, mUp, mDown;
    
    // Текущие углы поворота траектории
    private float mPan = 0.0f;
    private float mTilt = 0.0f;
    
    // Объект оценки позы камеры
    private Processor mProcessor;
    
    // Этот флаг выставляется на время работы алгоритма оценки позы камеры 
    volatile private boolean mInProcessing = false;

    // Запускается при создании активити
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.main_activity_view);

        // Создаём оъект для оценки поз
        mProcessor = new Processor();
        
        // Открываем камеру средствами OpenCV
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
        
        // Создаём объекты для отрисовки траектории средствами OpenGL
        mGlSurfaceView = new GLSurfaceView(this);
        mGlSurfaceView.setEGLContextClientVersion(2);
        mRenderer = new GlRenderer();
        mGlSurfaceView.setRenderer(mRenderer);
        mPosesView = (FrameLayout) findViewById(R.id.poses_image);
        mPosesView.addView(mGlSurfaceView);
        
        // =================================================
        // Кнопки вращения траектории камеры
        mLeft = (ImageButton)findViewById(R.id.left);
        mLeft.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            mPan += 10f;
            mRenderer.rotatePath(mPan, mTilt);
          }
        });

        mRight = (ImageButton)findViewById(R.id.right);
        mRight.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            mPan -= 10f;
            mRenderer.rotatePath(mPan, mTilt);
          }
        });
        
        mUp = (ImageButton)findViewById(R.id.up);
        mUp.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            mTilt -= 10f;
            mRenderer.rotatePath(mPan, mTilt);
          }
        });

        mDown = (ImageButton)findViewById(R.id.down);
        mDown.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            mTilt += 10f;
            mRenderer.rotatePath(mPan, mTilt);
          }
        });
        
    }
    
    // Обработчик кадров с камеры
    @Override
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        Mat rgba = inputFrame.rgba();
        
        // Если предыдущие вычисления закончены - 
        //     запускаем оценку позы для нового кадра
        if(!mInProcessing) {
          mInProcessing = true;
          
          new PoseEstimator().execute(rgba); 
        }
        
        return rgba;
    }

    
    // Оценка позы камеры в другом потоке
    private class PoseEstimator extends AsyncTask<Mat, Void, Void> {
      @Override
      protected Void doInBackground(Mat... arg0) {
        
        Mat tvec = mProcessor.findPose(arg0[0]);
        
        if(!tvec.empty()) {
          mRenderer.addPose(tvec);
        }
        
        mInProcessing = false;
        
        return null;
      }
    }

    // Стандартный загрузчик библиотеки OpenCV
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
      @Override
      public void onManagerConnected(int status) {
          switch (status) {
              case LoaderCallbackInterface.SUCCESS:
              {
                  Log.i(TAG, "OpenCV loaded successfully");
                  mOpenCvCameraView.enableView();
              } break;
              default:
              {
                  super.onManagerConnected(status);
              } break;
          }
      }
  };
  
  // ==================================================
  // События жизненного цикла приложения
  @Override
  public void onPause()
  {
      super.onPause();
      if (mOpenCvCameraView != null)
          mOpenCvCameraView.disableView();
  }

  @Override
  public void onResume()
  {
      super.onResume();
      OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, 
          mLoaderCallback);
  }

  @Override
  public void onDestroy() {
      super.onDestroy();
      if (mOpenCvCameraView != null)
          mOpenCvCameraView.disableView();
  }
  
  @Override
  public void onCameraViewStarted(int width, int height) {
  }

  @Override
  public void onCameraViewStopped() {
  }

}
