package com.samsung.atlas.ar_sample;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import org.opencv.core.Mat;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

public class GlRenderer implements Renderer {
  private static final String TAG = "GLRenderer";
  
  // Объект для отрисовки траектории
  private PosesDrawer poses;

  // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
  private final float[] mMVPMatrix = new float[16];
  private final float[] mProjectionMatrix = new float[16];
  private final float[] mViewMatrix = new float[16];
  private final float[] mRotationPan = new float[16];
  private final float[] mRotationTilt = new float[16];
  
  // Текущие углы поворота траеткории
  private float mPan = 0.0f;
  private float mTilt = 0.0f;

  
  // Событие отрисовки траектории
  @Override
  public void onDrawFrame(GL10 unused) {
    // Очищаем экран
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

    // Задаём позицию наблюдателя 
    Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

    // Вычисляем матрицу проектирования 
    Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
    
    // Поворачиваем траектории
    Matrix.setRotateM(mRotationPan, 0, mPan, 0, 1.0f, 0);
    float[] temp = new float[16];
    Matrix.multiplyMM(temp, 0, mMVPMatrix, 0, mRotationPan, 0);

    Matrix.setRotateM(mRotationTilt, 0, mTilt, 1.0f, 0, 0);
    float[] scratch = new float[16];
    Matrix.multiplyMM(scratch, 0, temp, 0, mRotationTilt, 0);

    // Рисуем траекторию
    poses.draw(scratch);
  }

  
  // ==============================================================
  // События жизненного цикла поверхности OpenGL
  @Override
  public void onSurfaceChanged(GL10 unused, int width, int height) {
    // Создаем вьюпорт по размеру окна
    GLES20.glViewport(0, 0, width, height);

    float ratio = (float) width / height;

    // Создаём матрицу проектирования
    Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
  }

  @Override
  public void onSurfaceCreated(GL10 unused, EGLConfig config) {

    // Задаём цвет очистки экрана
    GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    checkGlError("onSurfaceCreated");

    // Создаём объект для отрисовки траектории
    poses = new PosesDrawer();
  }
  // ==============================================================

  // Стандартный метод проверки ошибок в OpenGL
  private static void checkGlError(String glOperation) {
    int error;
    while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
        Log.e(TAG, glOperation + ": glError " + error);
        throw new RuntimeException(glOperation + ": glError " + error);
    }
  }
  
  // Вращение траектории
  public void rotatePath(float pan, float tilt) {
    mPan = pan;
    mTilt = tilt;
  }
  
  // Добавить новую позу к траектории
  public void addPose(Mat tvec) {
    if(poses != null)
      poses.addPose(tvec);
  }
}
