package com.samsung.atlas.ar_sample;

import org.opencv.core.*;
import org.opencv.calib3d.*;
import org.opencv.imgproc.Imgproc;

public class Processor {
  
  // https://ru.wikipedia.org/wiki/%D0%AD%D0%BA%D0%B2%D0%B8%D0%B2%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%BD%D0%BE%D0%B5_%D1%84%D0%BE%D0%BA%D1%83%D1%81%D0%BD%D0%BE%D0%B5_%D1%80%D0%B0%D1%81%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D0%B5
  private static final float FOCAL_LENGTH_IN_35_MM = 37.f;// Эквивалентное фокусное расстояние
  private static final float DIAG_35_MM = 43.27f;         // Диагональ 35-мм кадра

  // Вычисление (задание) трёхмерных координат центров кругов на паттерне
  private MatOfPoint3f createGrid() {
    Point3[] object_points= new Point3[4 * 11];
    for (int j=0; j<6; ++j)
        for (int i=0; i<4; ++i)
            object_points[j*8 + i] = new Point3 (i, j, 0);
    for (int j=0; j<5; ++j)
        for (int i=0; i<4; ++i)
            object_points[j*8 + i + 4] = new Point3 (0.5f + i, 0.5f + j, 0);
    
    MatOfPoint3f res = new MatOfPoint3f();
    res.fromArray(object_points);
    return res;    
  }
  
  // Создание матрица параметров камеры
  private Mat createCameraMatrix(int width, int height) {
    
    // Диагональ матрицы в пикселях
    float diag_in_px = (float) Math.sqrt(width * width + height * height);
    // Фокусное растояние камеры в пикселях
    float focal_in_px = FOCAL_LENGTH_IN_35_MM / DIAG_35_MM * diag_in_px;

    Mat res = Mat.zeros(3, 3, CvType.CV_32F);// Заполнение нулями

    // Фокусное расстояние (в пикселях)
    res.put(0, 0, focal_in_px);
    res.put(1, 1, focal_in_px);
    res.put(2, 2, 1.f);
    // Положение оптического центра на матрице (в пикселях)
    res.put(0, 2, width / 2);
    res.put(1, 2, height / 2);
    
    return res;
  }
  
  // Функция вычисления положения и ориентации (позы) камеры 
  public Mat findPose(Mat rgba) {

    // Многие функции OpenCV работают только с черно-белым изображением - конвертируем
    Mat grayScale = new Mat();
    Imgproc.cvtColor(rgba, grayScale, Imgproc.COLOR_RGBA2GRAY);
    
    // Сюда будут записаны координаты найденных кругов паттерна
    MatOfPoint2f gridCentres = new MatOfPoint2f();
    // Размер (количество кругов) паттерна - 11 столбцов по 4 круга в каждом
    Size sz = new Size(4, 11);

    // Поиск паттерна на изображении
    boolean found = Calib3d.findCirclesGridDefault(grayScale, sz, gridCentres, 
        Calib3d.CALIB_CB_ASYMMETRIC_GRID);
    
    if(!found)
      return new Mat();
    
    // Создание матрицы камеры
    Mat cameraMatrix = createCameraMatrix(grayScale.cols(), grayScale.rows());
    // Задание истинных (мировых) координат кругов на патерне
    MatOfPoint3f object3dPoints = createGrid();
    
    // Сюда будут записаны три угла ориентации камеры
    Mat rvec = new Mat();
    // Сюда будет записано положение камеры (три координаты)
    Mat tvec = new Mat();
    found = Calib3d.solvePnP(object3dPoints, gridCentres, cameraMatrix, 
        new MatOfDouble(), rvec, tvec);
    if(!found)
      return new Mat();
    
    return tvec;

  }
}
